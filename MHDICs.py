from MHDDefs import *
import MHDInit
import numpy as np

Rho0 = 1.0
P0   = 0.1
B_para = 1.0
V_para = 0.0
alp = np.pi/3.0


#Vector potential (See Athena alfven wave test)
#https://www.astro.princeton.edu/~jstone/Athena/tests/cp-alfven-wave/cp-alfven.html#twod-mhd
#See also, https://arxiv.org/pdf/astro-ph/0501557.pdf (Section 3.3.2, equations 54-56)


def Ax(x,y,z):
    """ 
    Vector potential Ax(x,y,z)
    """
    pAx = B_para*np.sin(alp)*z
    return pAx 

def Ay(x,y,z):
    """
    Vector potentail Ay(x,y,z)
    """
    pAy = -1.0*B_para*np.cos(alp)*z + 0.1*np.sin(2*np.pi*(x*np.cos(alp)+y*np.sin(alp)))/(2*np.pi*np.cos(alp))

    return pAy
    
def Az(x,y,z,Lx=25.6,Ly=12.8):
    """
    Vector potential Az(x,y,z)
    Peturbation function for GEM reconnection probelm
    Default values for Lx = 25.6 and Ly=12.8 are utilized
    """
    pAz = 0.1*np.cos(2*np.pi*(x*np.cos(alp)+y*np.sin(alp)))/(2*np.pi)
    
    return pAz


"""
Initialize MHD variables
"""
def GetICs(x,y,z):
	# Calculate grids and indices
	# xc,yc,zc: cell centers
	# xi,yi,zi: i-face cetners where bi is defined
	# xj,yj,zj: j-face cetners where bj is defined
	# xk,yk,zk: k-face cetners where bk is defined
	# dx,dy,dz: lengths of each cell edge
	(xc,yc,zc,xi,yi,zi,xj,yj,zj,xk,yk,zk,dx,dy,dz)=MHDInit.Metrics(x,y,z,NO)

	# Define premitive Hydrodynamic variables at cell center
	rho = np.zeros(xc.shape)
	vx  = np.zeros(xc.shape)
	vy  = np.zeros(xc.shape)
	vz  = np.zeros(xc.shape)
	p   = np.zeros(xc.shape)

	# Define Magnetic fields at cell faces
	bi = np.zeros(xi.shape)
	bj = np.zeros(xj.shape)
	bk = np.zeros(xk.shape)

	#rho,vx,vy,vz,p,bi,bj,bk = GetICs_Alf(x,y,z)


	# 2-D Orzag-Tang vortex
	rho = rho+25.0/(np.pi*36.0)
	p   = p+5.0/(np.pi*12.0)
	vx  = -np.sin(2.0*np.pi*yc)
	vy  =  np.sin(2.0*np.pi*xc)
	bi  = -1.0/np.sqrt(4.0*np.pi)*np.sin(2*np.pi*yi)
	bj  = 1.0/np.sqrt(4.0*np.pi)*np.sin(2*np.pi*2*xj)

	return rho,vx,vy,vz,p,bi,bj,bk

def GetICs_Alf(x,y,z):
	# Define premitive Hydrodynamic variables at cell center
	rho = np.zeros(xc.shape)
	vx  = np.zeros(xc.shape)
	vy  = np.zeros(xc.shape)
	vz  = np.zeros(xc.shape)
	p   = np.zeros(xc.shape)

	# Define Magnetic fields at cell faces
	bi = np.zeros(xi.shape)
	bj = np.zeros(xj.shape)
	bk = np.zeros(xk.shape)

	rho[:,:,:] = Rho0
	p  [:,:,:] = P0

	#Do Vector Potential Integration for Bi, Bj, Bk
	LAi = np.zeros(x.shape)
	LAj = np.zeros(x.shape)
	LAk = np.zeros(x.shape)
	for i in range(NO2,nx+NO2):
	    xijk = x[i,NO2:-NO2+1,NO2:-NO2+1]
	    xip1 = x[i+1,NO2:-NO2+1,NO2:-NO2+1]
	    yijk = y[i,NO2:-NO2+1,NO2:-NO2+1]
	    yip1 = y[i+1,NO2:-NO2+1,NO2:-NO2+1]
	    zijk = z[i,NO2:-NO2+1,NO2:-NO2+1]
	    zip1 = z[i+1,NO2:-NO2+1,NO2:-NO2+1]
	    LAi[i,NO2:-NO2+1,NO2:-NO2+1]=((x[i+1,NO2:-NO2+1,NO2:-NO2+1]-
	                               x[i,NO2:-NO2+1,NO2:-NO2+1])*
	                              gaussLineInt(
	                              Ax,xip1,yip1,zip1,xijk,yijk,zijk))
	for j in range(NO2,ny+NO2):
	    xijk = x[NO2:-NO2,j,NO2:-NO2]
	    xjp1 = x[NO2:-NO2,j+1,NO2:-NO2]
	    yijk = y[NO2:-NO2,j,NO2:-NO2]
	    yjp1 = y[NO2:-NO2,j+1,NO2:-NO2]
	    zijk = z[NO2:-NO2,j,NO2:-NO2]
	    zjp1 = z[NO2:-NO2,j+1,NO2:-NO2]
	    LAj[NO2:-NO2,j,NO2:-NO2]=((y[NO2:-NO2,j+1,NO2:-NO2]-
	                               y[NO2:-NO2,j,NO2:-NO2])*
	                              gaussLineInt(
	                              Ay,xjp1,yjp1,zjp1,xijk,yijk,zijk))    
	for k in range(NO2,nz+NO2):
	    xijk = x[NO2:-NO2,NO2:-NO2,k]
	    xkp1 = x[NO2:-NO2,NO2:-NO2,k+1]
	    yijk = y[NO2:-NO2,NO2:-NO2,k]
	    ykp1 = y[NO2:-NO2,NO2:-NO2,k+1]
	    zijk = z[NO2:-NO2,NO2:-NO2,k]
	    zkp1 = z[NO2:-NO2,NO2:-NO2,k+1]
	    LAk[NO2:-NO2,NO2:-NO2,k]=((z[NO2:-NO2,NO2:-NO2,k+1]-
	                               z[NO2:-NO2,NO2:-NO2,k])*
	                              gaussLineInt(
	                              Az,xkp1,ykp1,zkp1,xijk,yijk,zijk))

	#Use edge integrals to calculate face flux
	bi[NO2:-NO2+1,NO2:-NO2,NO2:-NO2] = (LAj[NO2:-NO2+1,NO2:-NO2-1,NO2:-NO2-1] - 
	                                    LAj[NO2:-NO2+1,NO2:-NO2-1,NO2+1:-NO2] +
	                                    LAk[NO2:-NO2+1,NO2+1:-NO2,NO2:-NO2-1] -
	                                    LAk[NO2:-NO2+1,NO2:-NO2-1,NO2:-NO2-1])
	bj[NO2:-NO2:,NO2:-NO2+1,NO2:-NO2] = -(LAi[NO2:-NO2-1,NO2:-NO2+1,NO2:-NO2-1] -
	                                      LAi[NO2:-NO2-1,NO2:-NO2+1,NO2+1:-NO2] +
	                                      LAk[NO2+1:-NO2,NO2:-NO2+1,NO2:-NO2-1] -
	                                      LAk[NO2:-NO2-1,NO2:-NO2+1,NO2:-NO2-1])
	bk[NO2:-NO2:,NO2:-NO2,NO2:-NO2+1] = (LAi[NO2:-NO2-1,NO2:-NO2-1,NO2:-NO2+1] - 
	                                     LAi[NO2:-NO2-1,NO2+1:-NO2,NO2:-NO2+1] +
	                                     LAj[NO2+1:-NO2,NO2:-NO2-1,NO2:-NO2+1] -
	                                     LAj[NO2:-NO2-1,NO2:-NO2-1,NO2:-NO2+1])
	bi[NO2:-NO2+1,NO2:-NO2,NO2:-NO2] = (bi[NO2:-NO2+1,NO2:-NO2,NO2:-NO2]/
	                                    dy[NO2:-NO2+2,NO2:-NO2,NO2:-NO2]/
	                                    dz[NO2:-NO2+2,NO2:-NO2,NO2:-NO2])
	bj[NO2:-NO2:,NO2:-NO2+1,NO2:-NO2] = (bj[NO2:-NO2:,NO2:-NO2+1,NO2:-NO2]/
	                                     dz[NO2:-NO2,NO2:-NO2+2,NO2:-NO2]/
	                                     dx[NO2:-NO2,NO2:-NO2+2,NO2:-NO2])
	bk[NO2:-NO2:,NO2:-NO2,NO2:-NO2+1] = (bk[NO2:-NO2:,NO2:-NO2,NO2:-NO2+1]/
	                                     dx[NO2:-NO2,NO2:-NO2,NO2:-NO2+2]/
	                                     dy[NO2:-NO2,NO2:-NO2,NO2:-NO2+2])

def gaussLineInt(func,xa,ya,za,xb,yb,zb):
    """
    Integrate FX(x,y,z) over the line (xa,ya,za) to (xb,yb,zb).  This
    subroutine does Gaussian integration with the first twelve Legendre
    polynomials as the basis fuctions.  Abromowitz and Stegun page 916.
    """
    
    # Positive zeros of 12th order Legendre polynomial
    a = [0.1252334085,  0.3678314989,  0.5873179542,
        0.7699026741,  0.9041172563,  0.9815606342]
    # Gaussian Integration coefficients for a 12th order polynomial
    wt = [0.2491470458,  0.2334925365,  0.2031674267,
        0.1600783285,  0.1069393259,  0.0471753363]
        
    dx = (xb-xa)/2.0
    dy = (yb-ya)/2.0
    dz = (zb-za)/2.0
    xbar = (xb+xa)/2.0
    ybar = (yb+ya)/2.0
    zbar = (zb+za)/2.0
    
    sum = 0.0
    for i in range(len(a)):
        sum = sum + wt[i] * (
                            func(xbar+a[i]*dx,ybar+a[i]*dy,zbar+a[i]*dz)+
                            func(xbar-a[i]*dx,ybar-a[i]*dy,zbar-a[i]*dz)
                            )

    return 0.5*sum    
