from MHDDefs import *
import os
import h5py

"""
Output to file
"""
def WriteOut(outdir,outbase,step,Time,x,y,z,rho,vx,vy,vz,p,bi,bj,bk,bx,by,bz):

    #Open the HDF5 file
    hdf5Name = os.path.join(outdir,'%s-%06d.h5'%(outbase,step))
    hdf5file = h5py.File(hdf5Name,'w')
    #Add some global attributes
    hdf5file.attrs['NO'] = NO
    hdf5file.attrs['time'] = Time
    hdf5file.attrs['step'] = step
    #Write the Variables
    dset = hdf5file.create_dataset('X',data=x)
    dset = hdf5file.create_dataset('Y',data=y)
    dset = hdf5file.create_dataset('Z',data=z)
    dset = hdf5file.create_dataset('Rho',data=rho)
    dset = hdf5file.create_dataset('P',data=p)
    dset = hdf5file.create_dataset('Vx',data=vx)
    dset = hdf5file.create_dataset('Vy',data=vy)
    dset = hdf5file.create_dataset('Vz',data=vz)        
    dset = hdf5file.create_dataset('Bx',data=bx)
    dset = hdf5file.create_dataset('By',data=by)
    dset = hdf5file.create_dataset('Bz',data=bz)
    dset = hdf5file.create_dataset('Bi',data=bi)
    dset = hdf5file.create_dataset('Bj',data=bj)
    dset = hdf5file.create_dataset('Bk',data=bk)
    #Close the file
    hdf5file.close() 


"""
Convert flux to field
"""
def Bijk2Bxyz(bi,bj,bk):
# calculate bx, by, bz at cell center, 2nd order accurate, equation (36) in
# Lyon et al., [2004] (the 1/8 in Lyon et al., [2004] is actually a typo)
    bx = (bi[1:,:,:] + bi[:-1,:,:])/2.
    by = (bj[:,1:,:] + bj[:,:-1,:])/2.
    bz = (bk[:,:,1:] + bk[:,:,:-1])/2.    
    return bx,by,bz

"""
Compute the divergence
"""
def CalcDivB(bi,bj,bk,dx,dy,dz):
    divB =( ( (bi[NO2+1:-NO2,NO2:-NO2,NO2:-NO2] - 
               bi[       NO2:-NO2-1,NO2:-NO2,NO2:-NO2])*
               dy[NO2:-NO2,NO2:-NO2,NO2:-NO2]*dz[NO2:-NO2,NO2:-NO2,NO2:-NO2] +
              (bj[NO2:-NO2,NO2+1:-NO2,NO2:-NO2] - 
               bj[ NO2:-NO2,NO2:-NO2-1,NO2:-NO2])*
               dz[NO2:-NO2,NO2:-NO2,NO2:-NO2]*dx[NO2:-NO2,NO2:-NO2,NO2:-NO2] + 
              (bk[NO2:-NO2,NO2:-NO2,NO2+1:-NO2] - 
               bk[ NO2:-NO2,NO2:-NO2,NO2:-NO2-1])*
               dx[NO2:-NO2,NO2:-NO2,NO2:-NO2]*dy[NO2:-NO2,NO2:-NO2,NO2:-NO2] ) /
               dx[NO2:-NO2,NO2:-NO2,NO2:-NO2]/
               dy[NO2:-NO2,NO2:-NO2,NO2:-NO2]/
               dz[NO2:-NO2,NO2:-NO2,NO2:-NO2])
    return divB
"""
Compute the conserved variables and return them in a tuple
"""
def getConservedVariables(rho,vx,vy,vz,p,gamma):
    """
    Function computes the conserved form of the plasma parameters 
    Requries:
        rho,vx,vy,vz,p - plasma variables
        gamma - ratio of specific heats
    Returns:
        rho,rhovx,rhovy,rhovz,eng - plasma variables in conserved form
    """               
    import numpy as n
    rhovx = rho*vx
    rhovy = rho*vy
    rhovz = rho*vz
    eng= 0.5*rho*(vx**2+vy**2+vz**2)+p/(gamma-1)
    
    return (rho,rhovx,rhovy,rhovz,eng)
      
"""
Compute the timestep based upon CFL condition
"""
def getDT(rho,vx,vy,vz,bx,by,bz,p,gamma,dx,dy,dz,CFL):
    """
    Function computes the timestep based upon computed wave speeds and the 
    CFL condition 
    Requries:
        rho,vx,vy,vz,bx,by,bz,p - plasma variables and magnetic field vars
        gamma - ratio of specific heats
        dx,dy,dz - size of edges
        CFL - CFL condition parameter
    Returns:
        dt - smallest allowed timestep
    """               
    import numpy as n
    
    Vfluid = n.sqrt(vx**2+vy**2+vz**2)
    Btotal = n.sqrt(bx**2+by**2+bz**2)
    Valfvn = Btotal/n.sqrt(rho)
    Vsound = n.sqrt(gamma*p/rho)
    
    VCFL = Vfluid+n.sqrt(Valfvn**2+Vsound**2)
    dtCFL = CFL /(VCFL/dx+VCFL/dy+VCFL/dz)
    dt = dtCFL.min()
    
    return (dt)
      
     