
"""
Compute the left and right states
"""
def reconstruct_3D(rho_h,NO2,PDMB,direction,limiter_type='PDM'):
    """
    Function compoutes the left and right states at the 
    Requries:
        rho_h - parameter to reconstruct
        NO2 - Half numerical order
        PDMB - B parameter for PDM method
        direction - 1 for x, 2 for y, and 3 for z
        limiter_type - limiter_type of limiter to use in 
    Returns:
        rho_left,rho_right - left and right states
    """               
    import numpy as n
    
    if (direction==1):
        #8-th order reconstruction
      
        if ( limiter_type == '8th'):
            rho_left = (
                        -3*rho_h[NO2-4:-NO2-4+1,NO2:-NO2+1,NO2:-NO2+1]+
                        29*rho_h[NO2-3:-NO2-3+1,NO2:-NO2+1,NO2:-NO2+1]-
                        139*rho_h[NO2-2:-NO2-2+1,NO2:-NO2+1,NO2:-NO2+1]+
                        533*rho_h[NO2-1:-NO2-1+1,NO2:-NO2+1,NO2:-NO2+1]+
                        533*rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2:-NO2+1]-
                        139*rho_h[NO2+1:-NO2+1+1,NO2:-NO2+1,NO2:-NO2+1]+
                        29*rho_h[NO2+2:-NO2+2+1,NO2:-NO2+1,NO2:-NO2+1]-
                        3*rho_h[NO2+3:,NO2:-NO2+1,NO2:-NO2+1])/840
            rho_right = rho_left
        elif(limiter_type == 'PDM'):
            rho_interp = (
                        -3*rho_h[NO2-4:-NO2-4+1,NO2:-NO2+1,NO2:-NO2+1]+
                        29*rho_h[NO2-3:-NO2-3+1,NO2:-NO2+1,NO2:-NO2+1]-
                        139*rho_h[NO2-2:-NO2-2+1,NO2:-NO2+1,NO2:-NO2+1]+
                        533*rho_h[NO2-1:-NO2-1+1,NO2:-NO2+1,NO2:-NO2+1]+
                        533*rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2:-NO2+1]-
                        139*rho_h[NO2+1:-NO2+1+1,NO2:-NO2+1,NO2:-NO2+1]+
                        29*rho_h[NO2+2:-NO2+2+1,NO2:-NO2+1,NO2:-NO2+1]-
                        3*rho_h[NO2+3:,NO2:-NO2+1,NO2:-NO2+1])/840
            
            # rho_interp = (
            #                -1*rho_h[NO2-2:-NO2-2+1,NO2:-NO2+1,NO2:-NO2+1]+
            #                7*rho_h[NO2-1:-NO2-1+1,NO2:-NO2+1,NO2:-NO2+1]+
            #                7*rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2:-NO2+1]-
            #                rho_h[NO2+1:-NO2+1+1,NO2:-NO2+1,NO2:-NO2+1])/12
            # limiting
            (rho_left,rho_right)= PDM2(
                                    rho_h[NO2-2:-NO2-2+1,NO2:-NO2+1,NO2:-NO2+1],
                                    rho_h[NO2-1:-NO2-1+1,NO2:-NO2+1,NO2:-NO2+1],
                                    rho_h[NO2:-NO2+1    ,NO2:-NO2+1,NO2:-NO2+1],
                                    rho_h[NO2+1:-NO2+1+1,NO2:-NO2+1,NO2:-NO2+1],
                                    rho_interp,PDMB)
        elif(limiter_type == 'TVD'):
            (rho_left,rho_right)= MHDpy.TVD(
                                    rho_h[NO2-2:-NO2-2+1,NO2:-NO2+1,NO2:-NO2+1],
                                    rho_h[NO2-1:-NO2-1+1,NO2:-NO2+1,NO2:-NO2+1],
                                    rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2:-NO2+1],
                                    rho_h[NO2+1:-NO2+1+1,NO2:-NO2+1,NO2:-NO2+1])
        elif(limiter_type == 'WENO'):
            rho_left = MHDpy.WENO5(
                                    rho_h[NO2-3:-NO2-3+1,NO2:-NO2+1,NO2:-NO2+1],
                                    rho_h[NO2-2:-NO2-2+1,NO2:-NO2+1,NO2:-NO2+1],
                                    rho_h[NO2-1:-NO2-1+1,NO2:-NO2+1,NO2:-NO2+1],
                                    rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2:-NO2+1],
                                    rho_h[NO2+1:-NO2+1+1,NO2:-NO2+1,NO2:-NO2+1])
            rho_right = MHDpy.WENO5(
                                    rho_h[NO2+2:-NO2+2+1,NO2:-NO2+1,NO2:-NO2+1],
                                    rho_h[NO2+1:-NO2+1+1,NO2:-NO2+1,NO2:-NO2+1],
                                    rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2:-NO2+1],
                                    rho_h[NO2-1:-NO2-1+1,NO2:-NO2+1,NO2:-NO2+1],
                                    rho_h[NO2-2:-NO2-2+1,NO2:-NO2+1,NO2:-NO2+1])

        
    elif(direction==2):
   
        if ( limiter_type == '8th'):
            rho_left = (
                        -3*rho_h[NO2:-NO2+1,NO2-4:-NO2-4+1,NO2:-NO2+1]+
                        29*rho_h[NO2:-NO2+1,NO2-3:-NO2-3+1,NO2:-NO2+1]-
                        139*rho_h[NO2:-NO2+1,NO2-2:-NO2-2+1,NO2:-NO2+1]+
                        533*rho_h[NO2:-NO2+1,NO2-1:-NO2-1+1,NO2:-NO2+1]+
                        533*rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2:-NO2+1]-
                        139*rho_h[NO2:-NO2+1,NO2+1:-NO2+1+1,NO2:-NO2+1]+
                        29*rho_h[NO2:-NO2+1,NO2+2:-NO2+2+1,NO2:-NO2+1]-
                        3*rho_h[NO2:-NO2+1,NO2+3:,NO2:-NO2+1])/840
            rho_right = rho_left
        elif(limiter_type == 'PDM'):
            rho_interp = (
                        -3*rho_h[NO2:-NO2+1,NO2-4:-NO2-4+1,NO2:-NO2+1]+
                        29*rho_h[NO2:-NO2+1,NO2-3:-NO2-3+1,NO2:-NO2+1]-
                        139*rho_h[NO2:-NO2+1,NO2-2:-NO2-2+1,NO2:-NO2+1]+
                        533*rho_h[NO2:-NO2+1,NO2-1:-NO2-1+1,NO2:-NO2+1]+
                        533*rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2:-NO2+1]-
                        139*rho_h[NO2:-NO2+1,NO2+1:-NO2+1+1,NO2:-NO2+1]+
                        29*rho_h[NO2:-NO2+1,NO2+2:-NO2+2+1,NO2:-NO2+1]-
                        3*rho_h[NO2:-NO2+1,NO2+3:,NO2:-NO2+1])/840
            
            #rho_interp = (
            #          -1*rho_h[NO2:-NO2+1,NO2-2:-NO2-2+1,NO2:-NO2+1]+
            #          7*rho_h[NO2:-NO2+1,NO2-1:-NO2-1+1,NO2:-NO2+1]+
            #          7*rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2:-NO2+1]-
            #          rho_h[NO2:-NO2+1,NO2+1:-NO2+1+1,NO2:-NO2+1])/12
    
            (rho_left,rho_right)= PDM2(
                                    rho_h[NO2:-NO2+1,NO2-2:-NO2-2+1,NO2:-NO2+1],
                                    rho_h[NO2:-NO2+1,NO2-1:-NO2-1+1,NO2:-NO2+1],
                                    rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2:-NO2+1],
                                    rho_h[NO2:-NO2+1,NO2+1:-NO2+1+1,NO2:-NO2+1],
                                    rho_interp,PDMB)

        elif (limiter_type== 'TVD'):
            (rho_left,rho_right)= MHDpy.TVD(
                                    rho_h[NO2:-NO2+1,NO2-2:-NO2-2+1,NO2:-NO2+1],
                                    rho_h[NO2:-NO2+1,NO2-1:-NO2-1+1,NO2:-NO2+1],
                                    rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2:-NO2+1],
                                    rho_h[NO2:-NO2+1,NO2+1:-NO2+1+1,NO2:-NO2+1])
        elif(limiter_type == 'WENO'):
            rho_left = MHDpy.WENO5(
                                    rho_h[NO2:-NO2+1,NO2-3:-NO2-3+1,NO2:-NO2+1],
                                    rho_h[NO2:-NO2+1,NO2-2:-NO2-2+1,NO2:-NO2+1],
                                    rho_h[NO2:-NO2+1,NO2-1:-NO2-1+1,NO2:-NO2+1],
                                    rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2:-NO2+1],
                                    rho_h[NO2:-NO2+1,NO2+1:-NO2+1+1,NO2:-NO2+1])
            rho_right = MHDpy.WENO5(
                                    rho_h[NO2:-NO2+1,NO2+2:-NO2+2+1,NO2:-NO2+1],
                                    rho_h[NO2:-NO2+1,NO2+1:-NO2+1+1,NO2:-NO2+1],
                                    rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2:-NO2+1],
                                    rho_h[NO2:-NO2+1,NO2-1:-NO2-1+1,NO2:-NO2+1],
                                    rho_h[NO2:-NO2+1,NO2-2:-NO2-2+1,NO2:-NO2+1])

    elif(direction==3):
        if( limiter_type == '8th'):
            rho_left = (
                        -3*rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2-4:-NO2-4+1]+
                        29*rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2-3:-NO2-3+1]-
                        139*rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2-2:-NO2-2+1]+
                        533*rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2-1:-NO2-1+1]+
                        533*rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2:-NO2+1]-
                        139*rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2+1:-NO2+1+1]+
                        29*rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2+2:-NO2+2+1]-
                        3*rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2+3:])/840
            rho_right = rho_left
        elif(limiter_type == 'PDM'):
            rho_interp = (
                        -3*rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2-4:-NO2-4+1]+
                        29*rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2-3:-NO2-3+1]-
                        139*rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2-2:-NO2-2+1]+
                        533*rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2-1:-NO2-1+1]+
                        533*rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2:-NO2+1]-
                        139*rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2+1:-NO2+1+1]+
                        29*rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2+2:-NO2+2+1]-
                        3*rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2+3:])/840
            
            #rho_interp = (
            #            -1*rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2-2:-NO2-2+1]+
            #            7*rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2-1:-NO2-1+1]+
            #            7*rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2:-NO2+1]-
            #            rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2+1:-NO2+1+1])/12
            
            (rho_left,rho_right)= PDM2(
                                    rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2-2:-NO2-2+1],
                                    rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2-1:-NO2-1+1],
                                    rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2:-NO2+1],
                                    rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2+1:-NO2+1+1],
                                    rho_interp,PDMB)
        elif(limiter_type == 'TVD'):
            (rho_left,rho_right)= MHDpy.TVD(
                                    rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2-2:-NO2-2+1],
                                    rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2-1:-NO2-1+1],
                                    rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2:-NO2+1],
                                    rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2+1:-NO2+1+1])
        elif(limiter_type == 'WENO'):
            rho_left = MHDpy.WENO5(
                                    rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2-3:-NO2-3+1],
                                    rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2-2:-NO2-2+1],
                                    rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2-1:-NO2-1+1],
                                    rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2:-NO2+1],
                                    rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2+1:-NO2+1+1])
            rho_right= MHDpy.WENO5(
                                    rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2+2:-NO2+2+1],
                                    rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2+1:-NO2+1+1],
                                    rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2:-NO2+1],
                                    rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2-1:-NO2-1+1],
                                    rho_h[NO2:-NO2+1,NO2:-NO2+1,NO2-2:-NO2-2+1])
    
    return (rho_left,rho_right)

"""
Compute average velocity at cell corner from cell centers
"""
def center2corner(vx,NO2,PDMB,direction,limiter_type='PDM'):
    """
    Function compoutes average values at cell corners from cell centers.
    Requries:
        vx - velocity
        NO2 - Half numerical order
        PDMB - B parameter for PDM method
        direction - type of plane must be xy,yz,zx
        limiter_type - type of limiter to use in 
    Returns:
        Ei,Ej,Ek - Elecrtic fields along cell edges
    """  
    import numpy as n

    ###########################################################################
    # calculate the averaged values at cell corners, from cell centers.
    # Detailed description see Lyon et al., [2004]. Note that the dimensions of
    # arrays in this function varies depending on the location of the values,
    # which can be tricky.
    #
    # MAIN ALGORITHM (in the x-y plane): 
    #   STEP 1: interpolate cell-centered values in the x-direction to cell
    #           faces
    #   STEP 2: interpolate the cell-faced values in the y-direction to cell
    #           cornor
    #   STEP 3: using limiters to split the interpolated cell-corner values to
    #           8 states in the four quadrants
    #   STEP 4: pick one state in each quadrants based on the larger relative
    #           differences in each direction
    #   STEP 5: average the 4 states in each quadrant to get an estimate of the
    #           average value at cell corner
    ###########################################################################

    
    if (direction == 'xy'):
        # step 1 & 2: interpolate velocity from cell center to corner
        #     first interpolate in the x-direction, so the index for
        #     x-dimension changes from ic_act to NO2:-NO2+1,for the y-direction,
        #     interpolation is done for the whole y-domain, i.e., J
        #     then interpolate in the y-direction, so the index for 
        #     y-dimension changes from J to jf_act
    

        if ( limiter_type == '8th'):   
            # interpolate vx from cell center to cell corner, 
            # vx_interp dimension is (NO2:-NO2+1, jf_act, kc_act]
            vx_interp_x = (
                -3*vx[NO2-4:-NO2-4+1,:,NO2:-NO2]+
                29*vx[NO2-3:-NO2-3+1,:,NO2:-NO2]-
                139*vx[NO2-2:-NO2-2+1,:,NO2:-NO2]+
                533*vx[NO2-1:-NO2-1+1,:,NO2:-NO2]+
                533*vx[NO2:-NO2+1,:,NO2:-NO2]-
                139*vx[NO2+1:-NO2+1+1,:,NO2:-NO2]+
                29*vx[NO2+2:-NO2+2+1,:,NO2:-NO2]-
                3*vx[NO2+3:,:,NO2:-NO2])/840
            vx_interp = (
                -3*vx_interp_x[:,NO2-4:-NO2-4+1,:]+
                29*vx_interp_x[:,NO2-3:-NO2-3+1,:]-
                139*vx_interp_x[:,NO2-2:-NO2-2+1,:]+
                533*vx_interp_x[:,NO2-1:-NO2-1+1,:]+
                533*vx_interp_x[:,NO2:-NO2+1,:]-
                139*vx_interp_x[:,NO2+1:-NO2+1+1,:]+
                29*vx_interp_x[:,NO2+2:-NO2+2+1,:]-
                3*vx_interp_x[:,NO2+3:,:])/840
            
            # split the interpolated velocity to 8-states, 
            # 2-states in each quadrant:
            # 00_x, 00_y, 10_x, 10_y, 01_x, 01_y, 11_x, 11_y
            vx_00_x = vx_interp
            vx_10_x = vx_interp
            vx_00_y = vx_interp
            vx_01_y = vx_interp
            vx_10_y = vx_interp
            vx_11_y = vx_interp
            vx_01_x = vx_interp
            vx_11_x = vx_interp
        elif (limiter_type == 'PDM'):
            # interpolate vx from cell center to cell corner, 
            # vx_interp dimension is (NO2:-NO2+1, jf_act, kc_act]
            vx_interp_x = (
                -3*vx[NO2-4:-NO2-4+1,:,NO2:-NO2]+
                29*vx[NO2-3:-NO2-3+1,:,NO2:-NO2]-
                139*vx[NO2-2:-NO2-2+1,:,NO2:-NO2]+
                533*vx[NO2-1:-NO2-1+1,:,NO2:-NO2]+
                533*vx[NO2:-NO2+1,:,NO2:-NO2]-
                139*vx[NO2+1:-NO2+1+1,:,NO2:-NO2]+
                29*vx[NO2+2:-NO2+2+1,:,NO2:-NO2]-
                3*vx[NO2+3:,:,NO2:-NO2])/840
            vx_interp = (
                -3*vx_interp_x[:,NO2-4:-NO2-4+1,:]+
                29*vx_interp_x[:,NO2-3:-NO2-3+1,:]-
                139*vx_interp_x[:,NO2-2:-NO2-2+1,:]+
                533*vx_interp_x[:,NO2-1:-NO2-1+1,:]+
                533*vx_interp_x[:,NO2:-NO2+1,:]-
                139*vx_interp_x[:,NO2+1:-NO2+1+1,:]+
                29*vx_interp_x[:,NO2+2:-NO2+2+1,:]-
                3*vx_interp_x[:,NO2+3:,:])/840
            # split the interpolated velocity to 8-states, 
            # 2-states in each quadrant:
            # 00_x, 00_y, 10_x, 10_y, 01_x, 01_y, 11_x, 11_y
            (vx_00_x,vx_10_x)=PDM2(
                                    vx[NO2-2:-NO2-2+1,NO2-1:-NO2-1+1,NO2:-NO2],
                                    vx[NO2-1:-NO2-1+1,NO2-1:-NO2-1+1,NO2:-NO2],
                                    vx[NO2:-NO2+1    ,NO2-1:-NO2-1+1,NO2:-NO2],
                                    vx[NO2+1:-NO2+1+1,NO2-1:-NO2-1+1,NO2:-NO2],
                                    vx_interp,PDMB)
            (vx_00_y,vx_01_y)= PDM2(
                                    vx[NO2-1:-NO2-1+1,NO2-2:-NO2-2+1,NO2:-NO2],
                                    vx[NO2-1:-NO2-1+1,NO2-1:-NO2-1+1,NO2:-NO2],
                                    vx[NO2-1:-NO2-1+1,NO2:-NO2+1    ,NO2:-NO2],
                                    vx[NO2-1:-NO2-1+1,NO2+1:-NO2+1+1,NO2:-NO2],
                                    vx_interp,PDMB)
            (vx_10_y,vx_11_y)= PDM2(
                                    vx[NO2:-NO2+1,NO2-2:-NO2-2+1,NO2:-NO2],
                                    vx[NO2:-NO2+1,NO2-1:-NO2-1+1,NO2:-NO2],
                                    vx[NO2:-NO2+1,NO2:-NO2+1    ,NO2:-NO2],
                                    vx[NO2:-NO2+1,NO2+1:-NO2+1+1,NO2:-NO2],
                                    vx_interp,PDMB)
            (vx_01_x,vx_11_x)= PDM2(
                                    vx[NO2-2:-NO2-2+1,NO2:-NO2+1,NO2:-NO2],
                                    vx[NO2-1:-NO2-1+1,NO2:-NO2+1,NO2:-NO2],
                                    vx[NO2:-NO2+1    ,NO2:-NO2+1,NO2:-NO2],
                                    vx[NO2+1:-NO2+1+1,NO2:-NO2+1,NO2:-NO2],
                                    vx_interp,PDMB)
        elif(limiter_type == 'TVD'):
            # put in TVD limiter here
            vx_interp = 0.25*(
                                vx[NO2-1:-NO2-1+1,NO2-1:-NO2-1+1,NO2:-NO2] + 
                                vx[NO2:-NO2+1    ,NO2-1:-NO2-1+1,NO2:-NO2] +
                                vx[NO2-1:-NO2-1+1,NO2:-NO2+1    ,NO2:-NO2] + 
                                vx[NO2:-NO2+1    ,NO2:-NO2+1    ,NO2:-NO2])
                                                
            (vx_00_x,vx_10_x)= MHDpy.TVD2(
                                vx[NO2-2:-NO2-2+1,NO2-1:-NO2-1+1,NO2:-NO2],
                                vx[NO2-1:-NO2-1+1,NO2-1:-NO2-1+1,NO2:-NO2],
                                vx[NO2:-NO2+1    ,NO2-1:-NO2-1+1,NO2:-NO2],
                                vx[NO2+1:-NO2+1+1,NO2-1:-NO2-1+1,NO2:-NO2],
                                vx_interp)
            (vx_00_y,vx_01_y)=MHDpy.TVD2(
                                vx[NO2-1:-NO2-1+1,NO2-2:-NO2-2+1,NO2:-NO2],
                                vx[NO2-1:-NO2-1+1,NO2-1:-NO2-1+1,NO2:-NO2],
                                vx[NO2-1:-NO2-1+1,NO2:-NO2+1    ,NO2:-NO2],
                                vx[NO2-1:-NO2-1+1,NO2+1:-NO2+1+1,NO2:-NO2],
                                vx_interp)
            (vx_10_y,vx_11_y)=MHDpy.TVD2(
                                vx[NO2:-NO2+1,NO2-2:-NO2-2+1,NO2:-NO2],
                                vx[NO2:-NO2+1,NO2-1:-NO2-1+1,NO2:-NO2],
                                vx[NO2:-NO2+1,NO2:-NO2+1    ,NO2:-NO2],
                                vx[NO2:-NO2+1,NO2+1:-NO2+1+1,NO2:-NO2],
                                vx_interp)
            (vx_01_x,vx_11_x)=MHDpy.TVD2(
                                vx[NO2-2:-NO2-2+1,NO2:-NO2+1,NO2:-NO2],
                                vx[NO2-1:-NO2-1+1,NO2:-NO2+1,NO2:-NO2],
                                vx[NO2:-NO2+1    ,NO2:-NO2+1,NO2:-NO2],
                                vx[NO2+1:-NO2+1+1,NO2:-NO2+1,NO2:-NO2],
                                vx_interp)
                
        # quadrant 00 - get the left states in both x and y direction
        # calculate the difference between x and y directions
        diff = n.sign(n.abs(vx[NO2-2:-NO2-2+1,NO2-1:-NO2-1+1,NO2:-NO2]-
                            vx[NO2-1:-NO2-1+1,NO2-1:-NO2-1+1,NO2:-NO2]) - 
                            n.abs(vx[NO2-1:-NO2-1+1,NO2-2:-NO2-2+1,NO2:-NO2]-
                            vx[NO2-1:-NO2-1+1,NO2-1:-NO2-1+1,NO2:-NO2]))
        # pick the state in the direction that has a bigger difference 
        # (if no difference then average the two states)
        vx_00 = ((1+diff)/2*vx_00_x +(1-diff)/2*vx_00_y)
        
        # quadrant 10 - get the left states in both x and y direction
        # calculate the difference between x and y directions
        diff = n.sign(n.abs(vx[NO2+1:-NO2+1+1,NO2-1:-NO2-1+1,NO2:-NO2]-
                            vx[NO2:-NO2+1    ,NO2-1:-NO2-1+1,NO2:-NO2]) - 
                            n.abs(vx[NO2:-NO2+1,NO2-2:-NO2-2+1,NO2:-NO2]-
                            vx[NO2:-NO2+1,NO2-1:-NO2-1+1,NO2:-NO2]))
        # pick the state that has a bigger difference 
        # (if no difference then average the two states)
        vx_10 = ((1+diff)/2*vx_10_x +(1-diff)/2*vx_10_y)
        
        # quadrant 01 - get the left states in both x and y direction
        # calculate the difference between x and y directions
        diff = n.sign(n.abs(vx[NO2-2:-NO2-2+1,NO2:-NO2+1,NO2:-NO2]-
                            vx[NO2-1:-NO2-1+1,NO2:-NO2+1,NO2:-NO2]) - 
                            n.abs(vx[NO2-1:-NO2-1+1,NO2+1:-NO2+1+1,NO2:-NO2]-
                            vx[NO2-1:-NO2-1+1,NO2:-NO2+1,NO2:-NO2]))
        # pick the state that has a bigger difference 
        # (if no difference then average the two states)
        vx_01 = ((1+diff)/2*vx_01_x +(1-diff)/2*vx_01_y)
        
        # quadrant 11 - get the left states in both x and y direction
        # calculate the difference between x and y directions
        diff = n.sign(n.abs(vx[NO2+1:-NO2+1+1,NO2:-NO2+1,NO2:-NO2]-
                            vx[NO2:-NO2+1    ,NO2:-NO2+1,NO2:-NO2]) - 
                            n.abs(vx[NO2:-NO2+1,NO2+1:-NO2+1+1,NO2:-NO2]-
                            vx[NO2:-NO2+1,NO2:-NO2+1,NO2:-NO2]))
        # pick the state that has a bigger difference 
        # (if no difference then average the two states)
        vx_11 = ((1+diff)/2*vx_11_x + (1-diff)/2*vx_11_y)
        
        # calculate average vx at the cell corner by averaging the four states
        vx_avg = 0.25*(vx_00 + vx_01 + vx_10 + vx_11)
        
        return vx_avg
        
    elif (direction == 'yz'): #
        # step 1 & 2: interpolate velocity from cell center to corner
        #         first interpolate in the y-direction, so the index for
        #         y-dimension changes from jc_act to jf_act,for the y-direction,
        #         interpolation is done for the whole z-domain, i.e., K
        #         then interpolate in the z-direction, so the index for 
        #         z-dimension changes from K to kf_act

        if ( limiter_type == '8th'):
            # interpolate vx from cell center to cell corner, 
            # vx_interp dimension is (ic_act, jf_act, kf_act]
            vx_interp_x = (
                -3*vx[NO2:-NO2,NO2-4:-NO2-4+1,:]+
                29*vx[NO2:-NO2,NO2-3:-NO2-3+1,:]-
                139*vx[NO2:-NO2,NO2-2:-NO2-2+1,:]+
                533*vx[NO2:-NO2,NO2-1:-NO2-1+1,:]+
                533*vx[NO2:-NO2,NO2:-NO2+1,:]-
                139*vx[NO2:-NO2,NO2+1:-NO2+1+1,:]+
                29*vx[NO2:-NO2,NO2+2:-NO2+2+1,:]-
                3*vx[NO2:-NO2,NO2+3:,:])/840
            vx_interp = (
                -3*vx_interp_x[:,:,NO2-4:-NO2-4+1]+
                29*vx_interp_x[:,:,NO2-3:-NO2-3+1]-
                139*vx_interp_x[:,:,NO2-2:-NO2-2+1]+
                533*vx_interp_x[:,:,NO2-1:-NO2-1+1]+
                533*vx_interp_x[:,:,NO2:-NO2+1]-
                139*vx_interp_x[:,:,NO2+1:-NO2+1+1]+
                29*vx_interp_x[:,:,NO2+2:-NO2+2+1]-
                3*vx_interp_x[:,:,NO2+3:])/840
            
            # split the interpolated velocity to 8-states, 
            # 2-states in each quadrant:
            # 00_x, 00_y, 10_x, 10_y, 01_x, 01_y, 11_x, 11_y
            vx_00_x = vx_interp
            vx_10_x = vx_interp
            vx_00_y = vx_interp
            vx_01_y = vx_interp
            vx_10_y = vx_interp
            vx_11_y = vx_interp
            vx_01_x = vx_interp
            vx_11_x = vx_interp
            
        elif( limiter_type == 'PDM'):
            # interpolate vx from cell center to cell corner, 
            # vx_interp dimension is (ic_act, jf_act, kf_act]
            vx_interp_x = (
                -3*vx[NO2:-NO2,NO2-4:-NO2-4+1,:]+
                29*vx[NO2:-NO2,NO2-3:-NO2-3+1,:]-
                139*vx[NO2:-NO2,NO2-2:-NO2-2+1,:]+
                533*vx[NO2:-NO2,NO2-1:-NO2-1+1,:]+
                533*vx[NO2:-NO2,NO2:-NO2+1,:]-
                139*vx[NO2:-NO2,NO2+1:-NO2+1+1,:]+
                29*vx[NO2:-NO2,NO2+2:-NO2+2+1,:]-
                3*vx[NO2:-NO2,NO2+3:,:])/840
            vx_interp = (
                -3*vx_interp_x[:,:,NO2-4:-NO2-4+1]+
                29*vx_interp_x[:,:,NO2-3:-NO2-3+1]-
                139*vx_interp_x[:,:,NO2-2:-NO2-2+1]+
                533*vx_interp_x[:,:,NO2-1:-NO2-1+1]+
                533*vx_interp_x[:,:,NO2:-NO2+1]-
                139*vx_interp_x[:,:,NO2+1:-NO2+1+1]+
                29*vx_interp_x[:,:,NO2+2:-NO2+2+1]-
                3*vx_interp_x[:,:,NO2+3:])/840
            
            # split the interpolated velocity to 8-states, 
            # 2-states in each quadrant:
            # 00_x, 00_y, 10_x, 10_y, 01_x, 01_y, 11_x, 11_y
            (vx_00_x,vx_10_x)= PDM2(
                                vx[NO2:-NO2,NO2-2:-NO2-2+1,NO2-1:-NO2-1+1],
                                vx[NO2:-NO2,NO2-1:-NO2-1+1,NO2-1:-NO2-1+1],
                                vx[NO2:-NO2,NO2:-NO2+1    ,NO2-1:-NO2-1+1],
                                vx[NO2:-NO2,NO2+1:-NO2+1+1,NO2-1:-NO2-1+1],
                                vx_interp,PDMB)
            (vx_00_y,vx_01_y)= PDM2(
                                vx[NO2:-NO2,NO2-1:-NO2-1+1,NO2-2:-NO2-2+1],
                                vx[NO2:-NO2,NO2-1:-NO2-1+1,NO2-1:-NO2-1+1],
                                vx[NO2:-NO2,NO2-1:-NO2-1+1,NO2:-NO2+1],
                                vx[NO2:-NO2,NO2-1:-NO2-1+1,NO2+1:-NO2+1+1],
                                vx_interp,PDMB)
            (vx_10_y,vx_11_y)= PDM2(
                                vx[NO2:-NO2,NO2:-NO2+1,NO2-2:-NO2-2+1],
                                vx[NO2:-NO2,NO2:-NO2+1,NO2-1:-NO2-1+1],
                                vx[NO2:-NO2,NO2:-NO2+1,NO2:-NO2+1],
                                vx[NO2:-NO2,NO2:-NO2+1,NO2+1:-NO2+1+1],
                                vx_interp,PDMB)
            (vx_01_x,vx_11_x)= PDM2(
                                vx[NO2:-NO2,NO2-2:-NO2-2+1,NO2:-NO2+1],
                                vx[NO2:-NO2,NO2-1:-NO2-1+1,NO2:-NO2+1],
                                vx[NO2:-NO2,NO2:-NO2+1    ,NO2:-NO2+1],
                                vx[NO2:-NO2,NO2+1:-NO2+1+1,NO2:-NO2+1],
                                vx_interp,PDMB)
        elif(limiter_type == 'TVD'):
            # put in TVD limiter here
            vx_interp = 0.25*(
                                vx[NO2:-NO2,NO2-1:-NO2-1+1,NO2-1:-NO2-1+1] + 
                                vx[NO2:-NO2,NO2:-NO2+1    ,NO2-1:-NO2-1+1] + 
                                vx[NO2:-NO2,NO2-1:-NO2-1+1,NO2:-NO2+1] + 
                                vx[NO2:-NO2,NO2:-NO2+1    ,NO2:-NO2+1])
                                                
            (vx_00_x,vx_10_x)= MHDpy.TVD2(
                                vx[NO2:-NO2,NO2-2:-NO2-2+1,NO2-1:-NO2-1+1],
                                vx[NO2:-NO2,NO2-1:-NO2-1+1,NO2-1:-NO2-1+1],
                                vx[NO2:-NO2,NO2:-NO2+1    ,NO2-1:-NO2-1+1],
                                vx[NO2:-NO2,NO2+1:-NO2+1+1,NO2-1:-NO2-1+1],
                                vx_interp)
            (vx_00_y,vx_01_y)= MHDpy.TVD2(
                                vx[NO2:-NO2,NO2-1:-NO2-1+1,NO2-2:-NO2-2+1],
                                vx[NO2:-NO2,NO2-1:-NO2-1+1,NO2-1:-NO2-1+1],
                                vx[NO2:-NO2,NO2-1:-NO2-1+1,NO2:-NO2+1],
                                vx[NO2:-NO2,NO2-1:-NO2-1+1,NO2+1:-NO2+1+1],
                                vx_interp)
            (vx_10_y,vx_11_y)= MHDpy.TVD2(
                                vx[NO2:-NO2,NO2:-NO2+1,NO2-2:-NO2-2+1],
                                vx[NO2:-NO2,NO2:-NO2+1,NO2-1:-NO2-1+1],
                                vx[NO2:-NO2,NO2:-NO2+1,NO2:-NO2+1],
                                vx[NO2:-NO2,NO2:-NO2+1,NO2+1:-NO2+1+1],
                                vx_interp)
            (vx_01_x,vx_11_x)= MHDpy.TVD2(
                                vx[NO2:-NO2,NO2-2:-NO2-2+1,NO2:-NO2+1],
                                vx[NO2:-NO2,NO2-1:-NO2-1+1,NO2:-NO2+1],
                                vx[NO2:-NO2,NO2:-NO2+1    ,NO2:-NO2+1],
                                vx[NO2:-NO2,NO2+1:-NO2+1+1,NO2:-NO2+1],
                                vx_interp)
            

        
        # quadrant 00 - get the left states in both y and z direction
        # calculate the difference between y and z directions

        diff = n.sign(n.abs(vx[NO2:-NO2,NO2-2:-NO2-2+1,NO2-1:-NO2-1+1]-
                            vx[NO2:-NO2,NO2-1:-NO2-1+1,NO2-1:-NO2-1+1]) - 
                            n.abs(vx[NO2:-NO2,NO2-1:-NO2-1+1,NO2-2:-NO2-2+1]-
                            vx[NO2:-NO2,NO2-1:-NO2-1+1,NO2-1:-NO2-1+1]))
        # pick the state in the direction that has a bigger difference 
        # (if no difference then average the two states)
        vx_00 = ((1+diff)/2*vx_00_x + (1-diff)/2*vx_00_y)
        
        # quadrant 10 - get the left states in both y and z direction
        # calculate the difference between y and z directions
        diff = n.sign(n.abs(vx[NO2:-NO2,NO2+1:-NO2+1+1,NO2-1:-NO2-1+1]-
                            vx[NO2:-NO2,NO2:-NO2+1    ,NO2-1:-NO2-1+1]) - 
                            n.abs(vx[NO2:-NO2,NO2:-NO2+1,NO2-2:-NO2-2+1]-
                            vx[NO2:-NO2,NO2:-NO2+1,NO2-1:-NO2-1+1]))
        # pick the state that has a bigger difference 
        # (if no difference then average the two states)
        vx_10 = ((1+diff)/2*vx_10_x + (1-diff)/2*vx_10_y)
        
        # quadrant 01 - get the left states in both y and z direction
        # calculate the difference between y and z directions
        diff = n.sign(n.abs(vx[NO2:-NO2,NO2-2:-NO2-2+1,NO2:-NO2+1]-
                            vx[NO2:-NO2,NO2-1:-NO2-1+1,NO2:-NO2+1]) - 
                            n.abs(vx[NO2:-NO2,NO2-1:-NO2-1+1,NO2+1:-NO2+1+1]-
                            vx[NO2:-NO2      ,NO2-1:-NO2-1+1,NO2:-NO2+1]))
        # pick the state that has a bigger difference 
        # (if no difference then average the two states)
        vx_01 = ((1+diff)/2*vx_01_x + (1-diff)/2*vx_01_y)
        
        # quadrant 11 - get the left states in both y and z direction
        # calculate the difference between y and z directions
        diff = n.sign(n.abs(vx[NO2:-NO2,NO2+1:-NO2+1+1,NO2:-NO2+1]-
                            vx[NO2:-NO2,NO2:-NO2+1    ,NO2:-NO2+1]) - 
                            n.abs(vx[NO2:-NO2,NO2:-NO2+1,NO2+1:-NO2+1+1]-
                            vx[NO2:-NO2      ,NO2:-NO2+1,NO2:-NO2+1]))
        # pick the state that has a bigger difference 
        # (if no difference then average the two states)
        vx_11 = ((1+diff)/2*vx_11_x + (1-diff)/2*vx_11_y)
        
        # calculate average vx at the cell corner by averaging the four states
        vx_avg = 0.25*(vx_00 + vx_01 + vx_10 + vx_11)
        return vx_avg
                                        
    elif (direction == 'zx'): # FIXME!!!!!! NOT tested yet..
        # step 1 & 2: interpolate velocity from cell center to corner
        #         first interpolate in the z-direction, so the index for
        #         z-dimension changes from kc_act to kf_act, for the x-direction,
        #         interpolation is done for the whole x-domain, i.e., J
        #         then interpolate in the x-direction, so the index for x-dimension
        #         changes from J to jf_act

        if(limiter_type == '8th'):
            # interpolate vx from cell center to cell corner, 
            # vx_interp dimension is (NO2:-NO2+1, jc_act, kf_act]
            
          
            vx_interp_x = (
                    -3*vx[:,NO2:-NO2,NO2-4:-NO2-4+1]+
                    29*vx[:,NO2:-NO2,NO2-3:-NO2-3+1]-
                    139*vx[:,NO2:-NO2,NO2-2:-NO2-2+1]+
                    533*vx[:,NO2:-NO2,NO2-1:-NO2-1+1]+
                    533*vx[:,NO2:-NO2,NO2:-NO2+1]-
                    139*vx[:,NO2:-NO2,NO2+1:-NO2+1+1]+
                    29*vx[:,NO2:-NO2,NO2+2:-NO2+2+1]-
                    3*vx[:,NO2:-NO2,NO2+3:])/840
            vx_interp = (
                    -3*vx_interp_x[NO2-4:-NO2-4+1,:,:]+
                    29*vx_interp_x[NO2-3:-NO2-3+1,:,:]-
                    139*vx_interp_x[NO2-2:-NO2-2+1,:,:]+
                    533*vx_interp_x[NO2-1:-NO2-1+1,:,:]+
                    533*vx_interp_x[NO2:-NO2+1,:,:]-
                    139*vx_interp_x[NO2+1:-NO2+1+1,:,:]+
                    29*vx_interp_x[NO2+2:-NO2+2+1,:,:]-
                    3*vx_interp_x[NO2+3:,:,:])/840
            
            # split the interpolated velocity to 8-states, 
            # 2-states in each quadrant:
            # 00_x, 00_y, 10_x, 10_y, 01_x, 01_y, 11_x, 11_y
            vx_00_x = vx_interp
            vx_10_x = vx_interp
            vx_00_y = vx_interp
            vx_01_y = vx_interp
            vx_10_y = vx_interp
            vx_11_y = vx_interp
            vx_01_x = vx_interp
            vx_11_x = vx_interp

        elif(limiter_type == 'PDM'):
            # interpolate vx from cell center to cell corner, 
            # vx_interp dimension is (NO2:-NO2+1, jc_act, kf_act]
            vx_interp_x = (
                    -3*vx[:,NO2:-NO2,NO2-4:-NO2-4+1]+
                    29*vx[:,NO2:-NO2,NO2-3:-NO2-3+1]-
                    139*vx[:,NO2:-NO2,NO2-2:-NO2-2+1]+
                    533*vx[:,NO2:-NO2,NO2-1:-NO2-1+1]+
                    533*vx[:,NO2:-NO2,NO2:-NO2+1]-
                    139*vx[:,NO2:-NO2,NO2+1:-NO2+1+1]+
                    29*vx[:,NO2:-NO2,NO2+2:-NO2+2+1]-
                    3*vx[:,NO2:-NO2,NO2+3:])/840
            vx_interp = (
                    -3*vx_interp_x[NO2-4:-NO2-4+1,:,:]+
                    29*vx_interp_x[NO2-3:-NO2-3+1,:,:]-
                    139*vx_interp_x[NO2-2:-NO2-2+1,:,:]+
                    533*vx_interp_x[NO2-1:-NO2-1+1,:,:]+
                    533*vx_interp_x[NO2:-NO2+1,:,:]-
                    139*vx_interp_x[NO2+1:-NO2+1+1,:,:]+
                    29*vx_interp_x[NO2+2:-NO2+2+1,:,:]-
                    3*vx_interp_x[NO2+3:,:,:])/840
            
            # split the interpolated velocity to 8-states, 
            # 2-states in each quadrant:
            # 00_x, 00_y, 10_x, 10_y, 01_x, 01_y, 11_x, 11_y
            (vx_00_x,vx_10_x) = PDM2(
                                    vx[NO2-1:-NO2-1+1,NO2:-NO2,NO2-2:-NO2-2+1],
                                    vx[NO2-1:-NO2-1+1,NO2:-NO2,NO2-1:-NO2-1+1],
                                    vx[NO2-1:-NO2-1+1,NO2:-NO2,NO2:-NO2+1],
                                    vx[NO2-1:-NO2-1+1,NO2:-NO2,NO2+1:-NO2+1+1],
                                    vx_interp,PDMB)
            (vx_00_y,vx_01_y)= PDM2(
                                    vx[NO2-2:-NO2-2+1,NO2:-NO2,NO2-1:-NO2-1+1],
                                    vx[NO2-1:-NO2-1+1,NO2:-NO2,NO2-1:-NO2-1+1],
                                    vx[NO2:-NO2+1    ,NO2:-NO2,NO2-1:-NO2-1+1],
                                    vx[NO2+1:-NO2+1+1,NO2:-NO2,NO2-1:-NO2-1+1],
                                    vx_interp,PDMB)
            (vx_10_y,vx_11_y)= PDM2(
                                    vx[NO2-2:-NO2-2+1,NO2:-NO2,NO2:-NO2+1],
                                    vx[NO2-1:-NO2-1+1,NO2:-NO2,NO2:-NO2+1],
                                    vx[NO2:-NO2+1    ,NO2:-NO2,NO2:-NO2+1],
                                    vx[NO2+1:-NO2+1+1,NO2:-NO2,NO2:-NO2+1],
                                    vx_interp,PDMB)
            (vx_01_x,vx_11_x)= PDM2(
                                    vx[NO2:-NO2+1,NO2:-NO2,NO2-2:-NO2-2+1],
                                    vx[NO2:-NO2+1,NO2:-NO2,NO2-1:-NO2-1+1],
                                    vx[NO2:-NO2+1,NO2:-NO2,NO2:-NO2+1],
                                    vx[NO2:-NO2+1,NO2:-NO2,NO2+1:-NO2+1+1],
                                    vx_interp,PDMB)
        elif(limiter_type == 'TVD'):
            # put in TVD limiter here
            vx_interp = 0.25*(
                            vx[NO2-1:-NO2-1+1,NO2:-NO2,NO2-1:-NO2-1+1] + 
                            vx[NO2-1:-NO2-1+1,NO2:-NO2,NO2:-NO2+1] + 
                            vx[NO2:-NO2+1    ,NO2:-NO2,NO2-1:-NO2-1+1] + 
                            vx[NO2:-NO2+1    ,NO2:-NO2,NO2:-NO2+1])
                                                
            (vx_00_x,vx_10_x)= MHDpy.TVD2(
                                    vx[NO2-1:-NO2-1+1,NO2:-NO2,NO2-2:-NO2-2+1],
                                    vx[NO2-1:-NO2-1+1,NO2:-NO2,NO2-1:-NO2-1+1],
                                    vx[NO2-1:-NO2-1+1,NO2:-NO2,NO2:-NO2+1],
                                    vx[NO2-1:-NO2-1+1,NO2:-NO2,NO2+1:-NO2+1+1],
                                    vx_interp)
            (vx_00_y,vx_01_y)= MHDpy.TVD2(
                                    vx[NO2-2:-NO2-2+1,NO2:-NO2,NO2-1:-NO2-1+1],
                                    vx[NO2-1:-NO2-1+1,NO2:-NO2,NO2-1:-NO2-1+1],
                                    vx[NO2:-NO2+1    ,NO2:-NO2,NO2-1:-NO2-1+1],
                                    vx[NO2+1:-NO2+1+1,NO2:-NO2,NO2-1:-NO2-1+1],
                                    vx_interp)
            (vx_10_y,vx_11_y)= MHDpy.TVD2(
                                    vx[NO2-2:-NO2-2+1,NO2:-NO2,NO2:-NO2+1],
                                    vx[NO2-1:-NO2-1+1,NO2:-NO2,NO2:-NO2+1],
                                    vx[NO2:-NO2+1    ,NO2:-NO2,NO2:-NO2+1],
                                    vx[NO2+1:-NO2+1+1,NO2:-NO2,NO2:-NO2+1],
                                    vx_interp)
            (vx_01_x,vx_11_x)= MHDpy.TVD2(
                                    vx[NO2:-NO2+1,NO2:-NO2,NO2-2:-NO2-2+1],
                                    vx[NO2:-NO2+1,NO2:-NO2,NO2-1:-NO2-1+1],
                                    vx[NO2:-NO2+1,NO2:-NO2,NO2:-NO2+1],
                                    vx[NO2:-NO2+1,NO2:-NO2,NO2+1:-NO2+1+1],
                                    vx_interp)

    
        # quadrant 00 - get the left states in both x and y direction
        # calculate the difference between x and y directions
        # need to declare a few more worker arrays
        diff = n.sign(n.abs(vx[NO2-1:-NO2-1+1,NO2:-NO2,NO2-2:-NO2-2+1]-
                            vx[NO2-1:-NO2-1+1,NO2:-NO2,NO2-1:-NO2-1+1]) - 
                            n.abs(vx[NO2-2:-NO2-2+1,NO2:-NO2,NO2-1:-NO2-1+1]-
                            vx[NO2-1:-NO2-1+1      ,NO2:-NO2,NO2-1:-NO2-1+1]))
        # pick the state in the direction that has a bigger difference 
        # (if no difference then average the two states)
        vx_00 = ((1+diff)/2*vx_00_x + (1-diff)/2*vx_00_y)
        
        # quadrant 10 - get the left states in both x and y direction
        # calculate the difference between x and y directions
        diff = n.sign(n.abs(vx[NO2-1:-NO2-1+1,NO2:-NO2,NO2+1:-NO2+1+1]-
                            vx[NO2-1:-NO2-1+1,NO2:-NO2,NO2:-NO2+1]) - 
                            n.abs(vx[NO2-2:-NO2-2+1,NO2:-NO2,NO2:-NO2+1]-
                            vx[NO2-1:-NO2-1+1      ,NO2:-NO2,NO2:-NO2+1]))
        # pick the state that has a bigger difference 
        # (if no difference then average the two states)
        vx_10 = ((1+diff)/2*vx_10_x + (1-diff)/2*vx_10_y)
        
        # quadrant 01 - get the left states in both x and y direction
        # calculate the difference between x and y directions
        diff = n.sign(n.abs(vx[NO2:-NO2+1,NO2:-NO2,NO2-2:-NO2-2+1]-
                            vx[NO2:-NO2+1,NO2:-NO2,NO2-1:-NO2-1+1]) - 
                            n.abs(vx[NO2+1:-NO2+1+1,NO2:-NO2,NO2-1:-NO2-1+1]-
                            vx[NO2:-NO2+1          ,NO2:-NO2,NO2-1:-NO2-1+1]))
        # pick the state that has a bigger difference 
        #(if no difference then average the two states)
        vx_01 = ((1+diff)/2*vx_01_x + (1-diff)/2*vx_01_y)
        
        # quadrant 11 - get the left states in both x and y direction
        # calculate the difference between x and y directions
        diff = n.sign(n.abs(vx[NO2:-NO2+1,NO2:-NO2,NO2+1:-NO2+1+1]-
                            vx[NO2:-NO2+1,NO2:-NO2,NO2:-NO2+1]) - 
                            n.abs(vx[NO2+1:-NO2+1+1,NO2:-NO2,NO2:-NO2+1]-
                            vx[NO2:-NO2+1          ,NO2:-NO2,NO2:-NO2+1]))
        # pick the state that has a bigger difference 
        #(if no difference then average the two states)
        vx_11 = ((1+diff)/2*vx_11_x + (1-diff)/2*vx_11_y)
        
        # calculate average vx at the cell corner by averaging the four states
        vx_avg = 0.25*(vx_00 + vx_01 + vx_10 + vx_11)
        return vx_avg

"""
Compute left and right PDM states
"""
def PDM2(f0,f1,f2,f3,f,PDMB):
    """
    Function compoutes the left and right PDM limited states 
    Requries:
        f0 - ?
        f1 - ?
        f2 - ?
        f3 - ?
        f - ?
        PDMB - value of the B parameter in the limiter 
    Returns:
        (f_left,f_right) - Limited left/right state values
    """  
    import numpy as n
    ##
    # f0=1
    # f1=1
    # f2=1
    # f3=0
    
    # second order interp
    # f=0.5*(f1+f2) 
    
    # fourth order interp
    # f = (-f0+9*f1+9*f2-f3)/16
    # f = (-f0+7*f1+7*f2-f3)/12
    
    #NB - the maximum and minimum functions do an element by element comparison
    #     of the maxtrix elements and return the corresponding max/min
    maxf = n.maximum(f1,f2)
    minf = n.minimum(f1,f2)
    
    f = n.maximum(minf,n.minimum(f,maxf))
    
    df0 = PDMB*(f1-f0)
    df1 = PDMB*(f2-f1)
    df2 = PDMB*(f3-f2)
    
    s0 = sign1(df0)
    s1 = sign1(df1)
    s2 = sign1(df2)
    
    df0 = n.abs(df0)
    df1 = n.abs(df1)
    df2 = n.abs(df2)
    
    q0 = n.abs(s0+s1)
    q1 = n.abs(s1+s2)
    
    df_left = f - f1
    df_righ = f2 - f
    
    f_left = f - s1*n.maximum(0,n.abs(df_left) - q0*df0)
    f_right= f + s1*n.maximum(0,n.abs(df_righ) - q1*df2) 
    
    return (f_left,f_right) 
    
"""
Compute return sign of number with zero replaced as 1
"""
def sign1(x):
    """
    Emulates fortran sign function by returning 1 for zero locations 
    Requries:
        x - array to check for signs
    Returns:
        a - array same size x with sign values
    """  
    import numpy as n
    
    a = n.sign(x)
    a[a == 0] = 1
    
    return a
    