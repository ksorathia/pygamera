###############################################################################
#
#             3-D ideal MHD Solver on a Stretched Cartesian Grid
#    
#    Numerical methods are described in Lyon et al., [2004]. Most of the
#    LFM schemes are used in this 3-D MHD code except:
#    1. No curvilinear grid
#    2. No Ring-average needed
#    3. No Boris-correction for high Alfven speed (very low plasma beta)
#    4. No Background B field subtraction (high order Gaussin integral)
#
#    MAIN FEATURES:
#    1. Finite Volume (actually finite difference in Cartesian)
#    2. Semi-conservative - use plasma energy equation
#    3. 2nd-8th order reconstruction with TVD/WENO/PDM, default PDM
#    4. 2nd order Adams-Bashforth time-stepping scheme
#    5. Operator-splitting for the Lorentz force terms
#    6. Gas-kinetic flux functions for fluid and magnetic stresses
#    7. High order Constraint transport (Yee-Grd) to conserve Div B = 0
#    8. Resistive MHD - to be implemented, relatively simple
#    9. Hall MHD - to be implemented, can use the getEk function
#
#    ALGORITHM:
#    STEP 1: Adams-Bashforth predictor step (half time step update)
#    STEP 2: Calculate E fields at cell edges
#    STEP 3: Calculate fluid flux/stresses and magnetic stresses
#            a) x-direction 
#            b) y-direction
#            c) z-direction
#    STEP 4: Update the hydrodynamic equation without magnetic stress
#    STEP 5: Apply magnetic stress to the updated momentum
#    STEP 6: Evolve B field 
#    STEP 7: Apply boundary conditions
#
#    NOTE:
#    The cell length should be included in the reconstruction process in
#    order to obtain formal accuracy, which is probably more important in
#    the curvilinear version of the solver. For uniform cartesian, doesn't
#    matter.
###############################################################################

import numpy as np
import pylab as pl
import time,os,h5py

import MHDInit
import MHDCalc
import MHDUtils
import MHDRecon
import MHDAdvance
import MHDICs
import BCs
from MHDDefs import *

# Model Parameters
imagedir = os.getcwd() # directory to store image files
outdir   = os.getcwd() # directory to store HDF5 files
imagebase = "gam" # base name of image files.
outbase   = "gam" # base name of HDF5 files.

outint = 50 #Frequency to dump HDF5 files
# Grid information- nx,ny,nz are # of ACTIVE cells (no ghost cell included)
# The generated grid are cell corners, the metric function will calculate
# cell centers, faces and other grid information
#NOTE: Getting nx,ny,nz,tFin from MHDDefs.py

#----
(x,y,z)=MHDInit.Generate_Grid_3D_uniform(nx,ny,nz,NO) # This function generate a 
                                               # uniformly distributed active 
                                               # grid between -1 and 1 with 
                                               # nx, ny nz active cells in each 
                                               # direction
(nx_total,ny_total,nz_total)=x.shape           # with NO/2 ghost cells, 
                                               # nx_total, ny_total, nz_total 
                                               # are total num of cell corners

#Remap the grid to 0,Lx and 0,Ly
#Lx = np.sqrt(5.0)
#Ly = np.sqrt(5.0)/2.0
Lx = 1.0
Ly = 1.0

x = (x+1.0)*Lx
y = (y+1.0)*Ly


# Calculate grids and indices
# xc,yc,zc: cell centers
# xi,yi,zi: i-face cetners where bi is defined
# xj,yj,zj: j-face cetners where bj is defined
# xk,yk,zk: k-face cetners where bk is defined
# dx,dy,dz: lengths of each cell edge
(xc,yc,zc,xi,yi,zi,xj,yj,zj,xk,yk,zk,dx,dy,dz)=MHDInit.Metrics(x,y,z,NO)

# Define premitive Hydrodynamic variables at cell center
rho = np.zeros(xc.shape)
vx  = np.zeros(xc.shape)
vy  = np.zeros(xc.shape)
vz  = np.zeros(xc.shape)
p   = np.zeros(xc.shape)

# Define Magnetic fields at cell faces
bi = np.zeros(xi.shape)
bj = np.zeros(xj.shape)
bk = np.zeros(xk.shape)

#Get initial conditions on MHD vars
rho,vx,vy,vz,p,bi,bj,bk = MHDICs.GetICs(x,y,z)

# set boundary conditions 
# For Orszag-Tang vortex simulation, use periopdic for both x and y
xbctype = 'PER'
ybctype = 'PER'
#zbctype = 'EXP'
zbctype = 'PER'
BCs.Boundaries(rho,p,vx,vy,vz,bi,bj,bk,NO,
                xtype=xbctype,ytype=ybctype,ztype=zbctype)
               
bx,by,bz = MHDUtils.Bijk2Bxyz(bi,bj,bk)

# Get conserved hydrodynamic variables
(rho,rhovx,rhovy,rhovz,eng) = MHDUtils.getConservedVariables(rho,vx,vy,vz,p,gamma)

# get the first dt for A-B time stepping
dt0 = MHDUtils.getDT(rho,vx,vy,vz,bx,by,bz,p,gamma,dx,dy,dz,CFL)

# Save the initial states for the first Adam-Bashforth time stepping
rho_p = rho
vx_p  = vx
vy_p  = vy
vz_p  = vz
p_p   = p
bx_p  = bx
by_p  = by
bz_p  = bz
bi_p  = bi
bj_p  = bj
bk_p  = bk

# MAIN LOOP
# simulation Time information
count = 0
Time  = 0
RealT = 0
step  = 0
imageNum=0
print("Starting ...")
while (Time < tFin):
    
    Tstart=time.time()
    dt = MHDUtils.getDT(rho,vx,vy,vz,bx,by,bz,p,gamma,dx,dy,dz,CFL)
    Time = Time + dt
    #[rho,rhovx,rhovy,rhovz,eng] = MHDUtils.getConservedVariables(rho,vx,vy,vz,
                                                    #  p,gamma)
    
    rho  ,vx  ,vy  ,vz  ,p  ,bi  ,bj  ,bk  ,bx  ,by  ,bz  , \
    rho_p,vx_p,vy_p,vz_p,p_p,bi_p,bj_p,bk_p,bx_p,by_p,bz_p = MHDAdvance.StepMHD \
                                                             (rho  ,vx  ,vy  ,vz  ,p  ,bi  ,bj  ,bk  ,bx  ,by  ,bz  ,
                                                             rho_p,vx_p,vy_p,vz_p,p_p,bi_p,bj_p,bk_p,bx_p,by_p,bz_p ,
                                                             dt0,dx,dy,dz,dt)

    BCs.Boundaries(rho,p,vx,vy,vz,bi,bj,bk,NO,
                    xtype=xbctype,ytype=ybctype,ztype=zbctype)  
                                            
    # calculate bx, by, bz at cell center, 2nd order accurate, since bi,
    # bj, bk are already modified use boundary conditions, no need bc here
    # for bx, by, bz
    bx,by,bz = MHDUtils.Bijk2Bxyz(bi,bj,bk)

    RealDT = time.time() - Tstart
    RealT = RealT + RealDT
    step = step +1
    # plot results
    if((step %50)==1):
        # check divB
        divB = MHDUtils.CalcDivB(bi,bj,bk,dx,dy,dz)
    
        pl.figure()
        pl.pcolor(np.squeeze(xc[NO2:-NO2,NO2:-NO2,NO2:-NO2]),
            np.squeeze(yc[NO2:-NO2,NO2:-NO2,NO2:-NO2]),
            np.squeeze(rho[NO2:-NO2,NO2:-NO2,NO2:-NO2]))
        pl.title('Simulation Time = %f' % Time)
        saveFigName = os.path.join(imagedir,'%s-%06d.png'%(imagebase,imageNum))
        print(saveFigName)
        
        pl.savefig(saveFigName,dpi=100)
        pl.close()
        imageNum = imageNum + 1
        print("Loop %d Sim Time = %f Real  Time = %f" % (step,Time,RealT))
        print("Sim DT = %f Real DT = %f " % (dt,RealDT))
        #print(" Max(divB) = %f " % np.max(np.abs(divB)))

    if((step % outint)==1):
        MHDUtils.WriteOut(outdir,outbase,step,Time,x,y,z,rho,vx,vy,vz,p,bi,bj,bk,bx,by,bz)


                                                                                                                                                                                                
