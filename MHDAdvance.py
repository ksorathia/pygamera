from MHDDefs import *
import MHDUtils
import MHDCalc
import MHDRecon

"""
Compute the Electric field components on the cell edges
"""
def StepMHD(rho  ,vx  ,vy  ,vz  ,p  ,bi  ,bj  ,bk  ,bx  ,by  ,bz  ,
            rho_p,vx_p,vy_p,vz_p,p_p,bi_p,bj_p,bk_p,bx_p,by_p,bz_p,
            dt0,dx,dy,dz,dt):

    
    [rho,rhovx,rhovy,rhovz,eng] = MHDUtils.getConservedVariables(rho,vx,vy,vz,
                                                                p,gamma)

    # Step 1: get the half time step values
    rho_h = rho + dt/dt0/2*(rho-rho_p)
    vx_h = vx + dt/dt0/2*(vx-vx_p)
    vy_h = vy + dt/dt0/2*(vy-vy_p)
    vz_h = vz + dt/dt0/2*(vz-vz_p)
    p_h = p + dt/dt0/2*(p-p_p)
    bx_h = bx + dt/dt0/2*(bx-bx_p)
    by_h = by + dt/dt0/2*(by-by_p)
    bz_h = bz + dt/dt0/2*(bz-bz_p)
    bi_h = bi + dt/dt0/2*(bi-bi_p)
    bj_h = bj + dt/dt0/2*(bj-bj_p)
    bk_h = bk + dt/dt0/2*(bk-bk_p)  
        
    rho0 = rho
    rhovx0 = rhovx
    rhovy0 = rhovy
    rhovz0 = rhovz
    eng0 = eng
    
    # save the current state vector for next AB time stepping
    rho_p = rho
    vx_p = vx
    vy_p = vy
    vz_p = vz
    bx_p = bx
    by_p = by
    bz_p = bz
    bi_p = bi
    bj_p = bj
    bk_p = bk    
    p_p= p
    dt0 = dt    


    #Step 2 Calculat the electric field, no resistivity term    
    (Ei,Ej,Ek) = MHDCalc.getEk(vx,vy,vz,rho,p,gamma,bi,bj,bk,bx,by,bz,
                            NO2,PDMB,limiter_type)
    
    # Step 3 Calculate fluid and magnetic flux/stresses
    # a) Calculate fluid flux/stress in the x direction
    # # reconstruct cell centered primitive variables to cell faces
    (rho_left, rho_right) = MHDRecon.reconstruct_3D(rho_h,NO2,PDMB,1,limiter_type)    
    (vx_left, vx_right)   = MHDRecon.reconstruct_3D(vx_h,NO2,PDMB,1,limiter_type)   
    (vy_left, vy_right)   = MHDRecon.reconstruct_3D(vy_h,NO2,PDMB,1,limiter_type)       
    (vz_left, vz_right)   = MHDRecon.reconstruct_3D(vz_h,NO2,PDMB,1,limiter_type)        
    (p_left, p_right)     = MHDRecon.reconstruct_3D(p_h,NO2,PDMB,1,limiter_type)   
    # use Gas-hydro flux flunction to calculate the net flux at cell faces
    # Here we use a Gaussian distribution, the temperature is fluid.
    # Can use waterbag in Lyon et al., [2004]. Results are very similar.
    (Frho_p,FrhoVx_p,FrhoVy_p,FrhoVz_p,Feng_p,_,_,_,_,_) = MHDCalc.getHydroFlux(
                            rho_left,vx_left,vy_left,vz_left,p_left,gamma,1)
    (_,_,_,_,_,Frho_n,FrhoVx_n,FrhoVy_n,FrhoVz_n,Feng_n) = MHDCalc.getHydroFlux(
                            rho_right,vx_right,vy_right,vz_right,p_right,gamma,1) 
    
    rho_flux_x = Frho_p + Frho_n
    vx_flux_x = FrhoVx_p + FrhoVx_n
    vy_flux_x = FrhoVy_p + FrhoVy_n
    vz_flux_x = FrhoVz_p + FrhoVz_n
    eng_flux_x = Feng_p + Feng_n
    
    # calculate magnetic stress in the x direction
    # Reconstruct the cell centered magnetic fields to cell faces
    (bx_left, bx_right) = MHDRecon.reconstruct_3D(bx_h,NO2,PDMB,1,limiter_type)   
    (by_left, by_right) = MHDRecon.reconstruct_3D(by_h,NO2,PDMB,1,limiter_type)
    (bz_left, bz_right) = MHDRecon.reconstruct_3D(bz_h,NO2,PDMB,1,limiter_type)
    # Magnetic distribution function is also Gaussian, the "temperature" is
    # fluid+magnetic
    (Bstress_x_p, Bstress_y_p, Bstress_z_p,_, _,_) = MHDCalc.getMagneticStress(
        rho_left,vx_left,vy_left,vz_left,p_left,bx_left,by_left,bz_left,1)
    (_,_,_,Bstress_x_n, Bstress_y_n, Bstress_z_n) = MHDCalc.getMagneticStress(
        rho_right,vx_right,vy_right,vz_right,p_right,bx_right,by_right,bz_right,1)
    
    BstressX_x = Bstress_x_p + Bstress_x_n
    BstressY_x = Bstress_y_p + Bstress_y_n
    BstressZ_x = Bstress_z_p + Bstress_z_n    
        
    #  b) Calculate Flux in the y direction    
    (rho_left, rho_right) = MHDRecon.reconstruct_3D(rho_h,NO2,PDMB,2,limiter_type)    
    (vx_left, vx_right)   = MHDRecon.reconstruct_3D(vx_h,NO2,PDMB,2,limiter_type)   
    (vy_left, vy_right)   = MHDRecon.reconstruct_3D(vy_h,NO2,PDMB,2,limiter_type)       
    (vz_left, vz_right)   = MHDRecon.reconstruct_3D(vz_h,NO2,PDMB,2,limiter_type)        
    (p_left, p_right)     = MHDRecon.reconstruct_3D(p_h,NO2,PDMB,2,limiter_type)   
    
    (Frho_py,FrhoVx_py,FrhoVy_py,FrhoVz_py,Feng_py,_,_,_,_,_) = MHDCalc.getHydroFlux(
                        rho_left,vx_left,vy_left,vz_left,p_left,gamma,2)
    (_,_,_,_,_,Frho_ny,FrhoVx_ny,FrhoVy_ny,FrhoVz_ny,Feng_ny) = MHDCalc.getHydroFlux(
                        rho_right,vx_right,vy_right,vz_right,p_right,gamma,2) 
    
    rho_flux_y = Frho_py + Frho_ny
    vx_flux_y = FrhoVx_py + FrhoVx_ny
    vy_flux_y = FrhoVy_py + FrhoVy_ny
    vz_flux_y = FrhoVz_py + FrhoVz_ny
    eng_flux_y = Feng_py + Feng_ny    
    
    # calculate magnetic stress in the Y direction
    (bx_left, bx_right) = MHDRecon.reconstruct_3D(bx_h,NO2,PDMB,2,limiter_type)   
    (by_left, by_right) = MHDRecon.reconstruct_3D(by_h,NO2,PDMB,2,limiter_type)       
    (bz_left, bz_right) = MHDRecon.reconstruct_3D(bz_h,NO2,PDMB,2,limiter_type)      
    
    (Bstress_x_p, Bstress_y_p, Bstress_z_p,_,_,_) = MHDCalc.getMagneticStress(
        rho_left,vx_left,vy_left,vz_left,p_left,bx_left,by_left,bz_left,2)
    (_,_,_,Bstress_x_n, Bstress_y_n, Bstress_z_n) = MHDCalc.getMagneticStress(
        rho_right,vx_right,vy_right,vz_right,p_right,bx_right,by_right,bz_right,2)
    
    BstressX_y = Bstress_x_p + Bstress_x_n
    BstressY_y = Bstress_y_p + Bstress_y_n
    BstressZ_y = Bstress_z_p + Bstress_z_n    
    
    # c) Calculate Flux in the z direction
    (rho_left, rho_right) = MHDRecon.reconstruct_3D(rho_h,NO2,PDMB,3,limiter_type)    
    (vx_left, vx_right)   = MHDRecon.reconstruct_3D(vx_h,NO2,PDMB,3,limiter_type)   
    (vy_left, vy_right)   = MHDRecon.reconstruct_3D(vy_h,NO2,PDMB,3,limiter_type)       
    (vz_left, vz_right)   = MHDRecon.reconstruct_3D(vz_h,NO2,PDMB,3,limiter_type)        
    (p_left, p_right)     = MHDRecon.reconstruct_3D(p_h,NO2,PDMB,3,limiter_type)   
    
    (Frho_px,FrhoVx_px,FrhoVy_px,FrhoVz_px,Feng_px,_,_,_,_,_) = MHDCalc.getHydroFlux(
                    rho_left,vx_left,vy_left,vz_left,p_left,gamma,3)
    (_,_,_,_,_,Frho_nx,FrhoVx_nx,FrhoVy_nx,FrhoVz_nx,Feng_nx) = MHDCalc.getHydroFlux(
                    rho_right,vx_right,vy_right,vz_right,p_right,gamma,3) 
    
    rho_flux_z = Frho_px + Frho_nx
    vx_flux_z = FrhoVx_px + FrhoVx_nx
    vy_flux_z = FrhoVy_px + FrhoVy_nx
    vz_flux_z = FrhoVz_px + FrhoVz_nx
    eng_flux_z = Feng_px + Feng_nx        
    
    # calculate magnetic stress in the Z direction
    (bx_left, bx_right) = MHDRecon.reconstruct_3D(bx_h,NO2,PDMB,3,limiter_type)   
    (by_left, by_right) = MHDRecon.reconstruct_3D(by_h,NO2,PDMB,3,limiter_type)       
    (bz_left, bz_right) = MHDRecon.reconstruct_3D(bz_h,NO2,PDMB,3,limiter_type)      
    
    (Bstress_x_p, Bstress_y_p, Bstress_z_p,_,_,_) = MHDCalc.getMagneticStress(
        rho_left,vx_left,vy_left,vz_left,p_left,bx_left,by_left,bz_left,3)
    (_,_,_,Bstress_x_n, Bstress_y_n, Bstress_z_n) = MHDCalc.getMagneticStress(
        rho_right,vx_right,vy_right,vz_right,p_right,bx_right,by_right,bz_right,3)
    
    BstressX_z = Bstress_x_p + Bstress_x_n
    BstressY_z = Bstress_y_p + Bstress_y_n
    BstressZ_z = Bstress_z_p + Bstress_z_n    
    
    # Step 4 update Hydro variables without magnetic stress - operator
    # splitting step withouth JxB force
    rho[NO2:-NO2,NO2:-NO2,NO2:-NO2] = (rho0[NO2:-NO2,NO2:-NO2,NO2:-NO2] - 
        dt/dx[NO2:-NO2,NO2:-NO2,NO2:-NO2]*(rho_flux_x[1:,:-1,:-1]-
        rho_flux_x[:-1,:-1,:-1])- 
        dt/dy[NO2:-NO2,NO2:-NO2,NO2:-NO2]*(rho_flux_y[:-1,1:,:-1]-
        rho_flux_y[:-1,:-1,:-1]) -
        dt/dz[NO2:-NO2,NO2:-NO2,NO2:-NO2]*(rho_flux_z[:-1,:-1,1:]-
        rho_flux_z[:-1,:-1,:-1]) )
    rhovx[NO2:-NO2,NO2:-NO2,NO2:-NO2] = (rhovx0[NO2:-NO2,NO2:-NO2,NO2:-NO2] - 
        dt/dx[NO2:-NO2,NO2:-NO2,NO2:-NO2]*(vx_flux_x[1:,:-1,:-1]-
        vx_flux_x[:-1,:-1,:-1]) - 
        dt/dy[NO2:-NO2,NO2:-NO2,NO2:-NO2]*(vx_flux_y[:-1,1:,:-1]-
        vx_flux_y[:-1,:-1,:-1]) - 
        dt/dz[NO2:-NO2,NO2:-NO2,NO2:-NO2]*(vx_flux_z[:-1,:-1,1:]-
        vx_flux_z[:-1,:-1,:-1]) )
    rhovy[NO2:-NO2,NO2:-NO2,NO2:-NO2] = (rhovy0[NO2:-NO2,NO2:-NO2,NO2:-NO2] - 
        dt/dx[NO2:-NO2,NO2:-NO2,NO2:-NO2]*(vy_flux_x[1:,:-1,:-1]-
        vy_flux_x[:-1,:-1,:-1]) - 
        dt/dy[NO2:-NO2,NO2:-NO2,NO2:-NO2]*(vy_flux_y[:-1,1:,:-1]-
        vy_flux_y[:-1,:-1,:-1]) - 
        dt/dz[NO2:-NO2,NO2:-NO2,NO2:-NO2]*(vy_flux_z[:-1,:-1,1:]-
        vy_flux_z[:-1,:-1,:-1]) )
    rhovz[NO2:-NO2,NO2:-NO2,NO2:-NO2] = (rhovz0[NO2:-NO2,NO2:-NO2,NO2:-NO2] - 
        dt/dx[NO2:-NO2,NO2:-NO2,NO2:-NO2]*(vz_flux_x[1:,:-1,:-1]-
        vz_flux_x[:-1,:-1,:-1]) - 
        dt/dy[NO2:-NO2,NO2:-NO2,NO2:-NO2]*(vz_flux_y[:-1,1:,:-1]-
        vz_flux_y[:-1,:-1,:-1]) - 
        dt/dz[NO2:-NO2,NO2:-NO2,NO2:-NO2]*(vz_flux_z[:-1,:-1,1:]-
        vz_flux_z[:-1,:-1,:-1]) )
    eng[NO2:-NO2,NO2:-NO2,NO2:-NO2] = (eng0[NO2:-NO2,NO2:-NO2,NO2:-NO2] - 
        dt/dx[NO2:-NO2,NO2:-NO2,NO2:-NO2]*(eng_flux_x[1:,:-1,:-1]-
        eng_flux_x[:-1,:-1,:-1]) - 
        dt/dy[NO2:-NO2,NO2:-NO2,NO2:-NO2]*(eng_flux_y[:-1,1:,:-1]-
        eng_flux_y[:-1,:-1,:-1]) - 
        dt/dz[NO2:-NO2,NO2:-NO2,NO2:-NO2]*(eng_flux_z[:-1,:-1,1:]-
        eng_flux_z[:-1,:-1,:-1]) )
    
    # get plasma pressure - now rho and p are solved (to O(dt)?)
    vx = rhovx/rho
    vy = rhovy/rho
    vz = rhovz/rho
    p = (eng - 0.5*rho*(vx**2+vy**2+vz**2))*(gamma-1)
    
    # Step 5 apply the magnetic stress to the momentums - operator 
    # splitting step dealing with JxB force Since JxB doesn't heat the
    # plasma, pressure is not changed between the Step 4 and 5
    rhovx[NO2:-NO2,NO2:-NO2,NO2:-NO2] = (rhovx[NO2:-NO2,NO2:-NO2,NO2:-NO2] - 
        dt/dx[NO2:-NO2,NO2:-NO2,NO2:-NO2]*(BstressX_x[1:,:-1,:-1]-
        BstressX_x[:-1,:-1,:-1]) - 
        dt/dy[NO2:-NO2,NO2:-NO2,NO2:-NO2]*(BstressX_y[:-1,1:,:-1]-
        BstressX_y[:-1,:-1,:-1]) - 
        dt/dz[NO2:-NO2,NO2:-NO2,NO2:-NO2]*(BstressX_z[:-1,:-1,1:]-
        BstressX_z[:-1,:-1,:-1]) )
    rhovy[NO2:-NO2,NO2:-NO2,NO2:-NO2] = (rhovy[NO2:-NO2,NO2:-NO2,NO2:-NO2] - 
        dt/dx[NO2:-NO2,NO2:-NO2,NO2:-NO2]*(BstressY_x[1:,:-1,:-1]-
        BstressY_x[:-1,:-1,:-1]) - 
        dt/dy[NO2:-NO2,NO2:-NO2,NO2:-NO2]*(BstressY_y[:-1,1:,:-1]-
        BstressY_y[:-1,:-1,:-1]) - 
        dt/dz[NO2:-NO2,NO2:-NO2,NO2:-NO2]*(BstressY_z[:-1,:-1,1:]-
        BstressY_z[:-1,:-1,:-1]) )
    rhovz[NO2:-NO2,NO2:-NO2,NO2:-NO2] = (rhovz[NO2:-NO2,NO2:-NO2,NO2:-NO2] - 
        dt/dx[NO2:-NO2,NO2:-NO2,NO2:-NO2]*(BstressZ_x[1:,:-1,:-1]-
        BstressZ_x[:-1,:-1,:-1]) - 
        dt/dy[NO2:-NO2,NO2:-NO2,NO2:-NO2]*(BstressZ_y[:-1,1:,:-1]-
        BstressZ_y[:-1,:-1,:-1]) - 
        dt/dz[NO2:-NO2,NO2:-NO2,NO2:-NO2]*(BstressZ_z[:-1,:-1,1:]-
        BstressZ_z[:-1,:-1,:-1]) )
    
    # get velocities with magnetic stress - now vx, vy, vz are solved
    vx = rhovx/rho
    vy = rhovy/rho
    vz = rhovz/rho
        
    # Step 6 update face-center magnetic fields (bi,bj,bk) through Faraday's law
    bi[NO2:-NO2,NO2:-NO2,NO2:-NO2] = bi[NO2:-NO2,NO2:-NO2,NO2:-NO2] - dt*( 
        1/dy[NO2:-NO2+1,NO2:-NO2,NO2:-NO2]*(Ek[:,1:,:]-Ek[:,:-1,:]) -
        1/dz[NO2:-NO2+1,NO2:-NO2,NO2:-NO2]*(Ej[:,:,1:]-Ej[:,:,:-1]) )
    bj[NO2:-NO2,NO2:-NO2,NO2:-NO2] = bj[NO2:-NO2,NO2:-NO2,NO2:-NO2] - dt*( 
        1/dz[NO2:-NO2,NO2:-NO2+1,NO2:-NO2]*(Ei[:,:,1:]-Ei[:,:,:-1])-
        1/dx[NO2:-NO2,NO2:-NO2+1,NO2:-NO2]*(Ek[1:,:,:]-Ek[:-1,:,:]) )
    bk[NO2:-NO2,NO2:-NO2,NO2:-NO2] = bk[NO2:-NO2,NO2:-NO2,NO2:-NO2] - dt*( 
        1/dx[NO2:-NO2,NO2:-NO2,NO2:-NO2+1]*(Ej[1:,:,:]-Ej[:-1,:,:])-
        1/dy[NO2:-NO2,NO2:-NO2,NO2:-NO2+1]*(Ei[:,1:,:]-Ei[:,:-1,:]) )

    return rho  ,vx  ,vy  ,vz  ,p  ,bi  ,bj  ,bk  ,bx  ,by  ,bz  ,rho_p,vx_p,vy_p,vz_p,p_p,bi_p,bj_p,bk_p,bx_p,by_p,bz_p