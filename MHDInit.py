"""
Function for generating a Uniform 3D Cartesian grid from -1 to 1 in all 
directions
"""
def Generate_Grid_3D_uniform(nx,ny,nz,NO):
    """
    Function for generating a Uniform 3D Cartesian grid from -1 to 1 in all 
    directions.  
    Requries:
        nx - number of cell corners in the x direction
        ny - number of cell corners in the y direction
        nz - number of cell corners in the z direction
        NO - order of numerical scheme to be applied
    Returns:
        (x,y,z) - arrays of 
            
    """
    import numpy as n 
    
    # Note it is important to specify these as floats to avoind int math errors  
    xmin=-1.
    xmax=1.
    ymin=-1.
    ymax = 1.
    zmin =-1.
    zmax = 1.
    
    NO2=NO/2
    
    dx = (xmax-xmin)/nx
    dy = (ymax-ymin)/ny
    dz = (zmax-zmin)/nz
    
    #NB - The use of NO2+1 ensures equal number of gurad cells on each side
    x_grid_all = n.arange((xmin-NO2*dx),(xmax+(NO2+1)*dx),dx) # x loc of cell faces
    y_grid_all = n.arange((ymin-NO2*dy),(ymax+(NO2+1)*dy),dy) # x loc of cell faces
    z_grid_all = n.arange((zmin-NO2*dz),(zmax+(NO2+1)*dz),dz) # x loc of cell faces
    
    X=n.zeros((n.size(x_grid_all),n.size(y_grid_all),n.size(z_grid_all)))
    Y=n.zeros((n.size(x_grid_all),n.size(y_grid_all),n.size(z_grid_all)))
    Z=n.zeros((n.size(x_grid_all),n.size(y_grid_all),n.size(z_grid_all)))
    
    for i in range(n.size(x_grid_all)):
        for j in range(n.size(y_grid_all)):
            for k in range(n.size(z_grid_all)):
                X[i,j,k] = x_grid_all[i]
                Y[i,j,k] = y_grid_all[j]
                Z[i,j,k] = z_grid_all[k]
    
    return (X,Y,Z)

"""
Function for computing metric parameters from grid
"""
def Metrics(x,y,z,NO):
    """
    Function for generating a Uniform 3D Cartesian grid from -1 to 1 in all 
    directions.  
    Requries:
        x - locations of cell corners in the x direction
        y - locations of cell corners in the y direction
        z - locations of cell corners in the z direction
        NO - order of numerical scheme to be applied
    Returns:
        xc,yc,zc - arrays of cell center locations 
        xi,yi,zi - i-face centers where bi is defined
        xj,yj,zj - j-face centers where bj is defined
        xk,yk,zk - k-face centers where bk is defined
        dx,dy,dz - lengths of each cell edge
        All are returned in a massive tuple   
    """

    # locations for cell centers
    xc = 0.125*( x[:-1,:-1,:-1] + x[:-1,:-1,1:] + 
                x[1:,:-1,1:] + x[1:,:-1,:-1] + 
                x[:-1,1:,:-1] + x[:-1,1:,1:] + 
                x[1:,1:,1:] + x[1:,1:,:-1] )
    yc = 0.125*( y[:-1,:-1,:-1] + y[:-1,:-1,1:] + 
                y[1:,:-1,1:] + y[1:,:-1,:-1] + 
                y[:-1,1:,:-1] + y[:-1,1:,1:] + 
                y[1:,1:,1:] + y[1:,1:,:-1] )
    zc = 0.125*( z[:-1,:-1,:-1] + z[:-1,:-1,1:] + 
                z[1:,:-1,1:] + z[1:,:-1,:-1] + 
                z[:-1,1:,:-1] + z[:-1,1:,1:] + 
                z[1:,1:,1:] + z[1:,1:,:-1] )

    # locations for i-face centers -  where bi is defined 
    xi = x[:,:-1,:-1]
    yi = 0.25*( y[:,:-1,:-1] + y[:,1:,:-1] + y[:,:-1,1:] + y[:,1:,1:] )
    zi = 0.25*( z[:,:-1,:-1] + z[:,1:,:-1] + z[:,:-1,1:] + z[:,1:,1:] )
    
    # locations for j-face centers - where bj is defined 
    xj = 0.25*( x[:-1,:,:-1] + x[1:,:,:-1] + x[:-1,:,1:] + x[1:,:,1:])
    yj = y[:-1,:,:-1];
    zj = 0.25*( z[:-1,:,:-1] + z[1:,:,:-1] + z[:-1,:,1:] + z[1:,:,1:])
    
    # locations for k-face centers - where bk is defined 
    xk = 0.25*( x[:-1,:-1,:] + x[1:,:-1,:] + x[:-1,1:,:] + x[1:,1:,:])
    yk = 0.25*( y[:-1,:-1,:] + y[1:,:-1,:] + y[:-1,1:,:] + y[1:,1:,:])
    zk = z[:-1,:-1,:]
    
    # lengths of each cell edges
    dx = x[1:,:-1,:-1] - x[:-1,:-1,:-1]
    dy = y[:-1,1:,:-1] - y[:-1,:-1,:-1]
    dz = z[:-1,:-1,1:] - z[:-1,:-1,:-1]
        
    return (xc,yc,zc,xi,yi,zi,xj,yj,zj,xk,yk,zk,dx,dy,dz)